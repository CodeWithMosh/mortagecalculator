import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import java.util.Scanner;

class CommandLineExample
{
    public static void main ( String [] arguments )
    {
        final byte PERCENT = 100;
        final byte MONTHS_IN_YEAR = 12;

        System.out.print("Input Principal: ");
        Scanner scanner = new Scanner(System.in);
        int principal = scanner.nextInt();
        System.out.print("Input Anual Interest Rate:");
        float monthlyInterestRate = (scanner.nextFloat()) / PERCENT / MONTHS_IN_YEAR;
        System.out.print(("Input the amount of years you'd like to pay: "));
        int monthsToPay = scanner.nextInt()  * MONTHS_IN_YEAR;

        double mortgage = principal
                * (monthlyInterestRate * Math.pow(1 + monthlyInterestRate, monthsToPay))
                / (Math.pow(1 + monthlyInterestRate, monthsToPay) - 1);

        NumberFormat currency = NumberFormat.getCurrencyInstance();
        currency.setCurrency(Currency.getInstance(Locale.US));
        System.out.println(currency.format(mortgage));
    }
}